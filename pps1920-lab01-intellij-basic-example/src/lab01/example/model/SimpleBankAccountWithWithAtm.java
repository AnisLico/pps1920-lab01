package lab01.example.model;

public class SimpleBankAccountWithWithAtm implements BankAccountWithAtm {

    private static final int ATMTAX = 1;
    private final BankAccount bankAccount;


    public SimpleBankAccountWithWithAtm(final BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }


    @Override
    public AccountHolder getHolder() {
        return this.bankAccount.getHolder();
    }

    @Override
    public double getBalance() {
        return this.bankAccount.getBalance();
    }

    @Override
    public void deposit(int usrID, double amount) {
        this.bankAccount.deposit(usrID, amount);
    }

    @Override
    public void withdraw(int usrID, double amount) {
        this.bankAccount.withdraw(usrID, amount);
    }

    @Override
    public void depositATM(int usrID, double amount) {
        this.bankAccount.deposit(usrID, amount - this.ATMTAX);
    }

    @Override
    public void withdrawATM(int usrID, double amount) {
        this.bankAccount.withdraw(usrID, amount + this.ATMTAX);
    }
}
