package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount {

    void depositATM(int usrID, double amount);
    void withdrawATM(int usrID, double amount);
}
