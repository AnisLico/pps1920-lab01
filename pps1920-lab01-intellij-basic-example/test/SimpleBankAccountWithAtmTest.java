import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleBankAccountWithAtmTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccount;
    private final static short ATMTAX = 1;

    @BeforeEach
    void setUp() {
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new SimpleBankAccountWithWithAtm(new SimpleBankAccount(accountHolder, 0));
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }


    @Test
    void testDeposit() {
        bankAccount.deposit(accountHolder.getId(), 100);
        assertEquals(100, bankAccount.getBalance());
    }

    @Test
    void testWrongDeposit() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.deposit(2, 50);
        assertEquals(100, bankAccount.getBalance());
    }

    @Test
    void testWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 70);
        assertEquals(30, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(2, 70);
        assertEquals(100, bankAccount.getBalance());
    }
    @Test
    void testDepositATM() {
        bankAccount.depositATM(accountHolder.getId(), 100);
        assertEquals(100 - ATMTAX, bankAccount.getBalance());
    }

    @Test
    void testWrongDepositATM() {
        bankAccount.depositATM(accountHolder.getId(), 100);
        bankAccount.depositATM(2, 50);
        assertEquals(100 - ATMTAX, bankAccount.getBalance());
    }

    @Test
    void testWithdrawATM() {
        bankAccount.depositATM(accountHolder.getId(), 100);
        bankAccount.withdrawATM(accountHolder.getId(), 70);
        assertEquals(30 - ATMTAX * 2, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdrawATM() {
        bankAccount.depositATM(accountHolder.getId(), 100);
        bankAccount.withdrawATM(2, 70);
        assertEquals(100 - ATMTAX , bankAccount.getBalance());
    }
}